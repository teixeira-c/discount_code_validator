# EKWATEST
## Discount code validator 

### The project
This project implements a symfony command which checks if a given discount code is valid. 

It also gives all the attached business offers to the given discount code.

### Installation
    composer install

### Usage
**&#35; We want to test the discount code "EKWA_WELCOME"**

    php bin/console discount-code:validate EKWA_WELCOME

**&#35; We want to test the discount code "BUZZ"**

    php bin/console discount-code:validate BUZZ
