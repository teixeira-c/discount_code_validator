<?php

namespace App\Client\Discount;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DiscountCodeClient
{
    const ERROR_FORMAT = 'cannot format response to array: ';
    const ERROR_RESPONSE = 'bad response code: ';

    /** @var HttpClientInterface */
    private $client;

    /** @var array */
    private $errors;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function fetchDiscountData(string $url): array
    {
        $response = $this->client->request(Request::METHOD_GET, $url);

        if (Response::HTTP_OK !== $responseCode = $response->getStatusCode()) {
            $this->errors[] = self::ERROR_RESPONSE.$responseCode;

            return [];
        }

        try {
            return $response->toArray();
        } catch (\Exception $e) {
            $this->errors[] = self::ERROR_FORMAT.$e->getMessage();

            return [];
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
