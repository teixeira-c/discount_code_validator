<?php

namespace App\Generator\Discount;

use App\Client\Discount\DiscountCodeClient;
use DiscountCodeDictionary;

class DiscountCodeOffersGenerator
{
    /** @var DiscountCodeClient */
    private $discountCodeClient;

    /** @var array */
    private $errors;

    public function __construct(DiscountCodeClient $discountCodeClient)
    {
        $this->discountCodeClient = $discountCodeClient;
    }

    public function generate(array $discountCodeData): array
    {
        $offersData = $this->discountCodeClient->fetchDiscountData($_ENV['URL_OFFERS_LIST']);
        if (empty($offersData)) {
            $this->errors = $this->discountCodeClient->getErrors();

            return [];
        }

        $discountCodeCompliantOffers = [DiscountCodeDictionary::COMPATIBLE_OFFERS_KEY => []];

        foreach ($offersData as $offer) {
            if (\array_key_exists(
                    DiscountCodeDictionary::VALID_DISCOUNT_CODES_LIST_KEY_REMOTE, $offer)
                && \in_array($discountCodeData[DiscountCodeDictionary::DISCOUNT_CODE_KEY], $offer[DiscountCodeDictionary::VALID_DISCOUNT_CODES_LIST_KEY_REMOTE])
            ) {
                $discountCodeCompliantOffers[DiscountCodeDictionary::COMPATIBLE_OFFERS_KEY][] = [
                    DiscountCodeDictionary::OFFER_NAME_KEY => $offer[DiscountCodeDictionary::OFFER_NAME_KEY_REMOTE],
                    DiscountCodeDictionary::OFFER_TYPE_KEY => $offer[DiscountCodeDictionary::OFFER_TYPE_KEY_REMOTE],
                ];
            }
        }

        return \array_merge($discountCodeData, $discountCodeCompliantOffers);
    }

    public function getErrors(): array
    {
        return $this->errors ?? [];
    }
}
