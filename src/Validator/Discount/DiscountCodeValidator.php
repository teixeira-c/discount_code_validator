<?php

namespace App\Validator\Discount;

use App\Client\Discount\DiscountCodeClient;
use DiscountCodeDictionary;

class DiscountCodeValidator
{
    /** @var DiscountCodeClient */
    private $discountCodeClient;

    /** @var array */
    private $discountCodeData;

    /** @var string */
    private $nonValidReason;

    /** @var array */
    private $errors;

    public function __construct(DiscountCodeClient $discountCodeClient)
    {
        $this->discountCodeClient = $discountCodeClient;
    }

    public function validate(string $discountCode): bool
    {
        $this->generateDiscountCodeData($discountCode);

        return !empty($this->discountCodeData);
    }

    public function getDiscountCodeData(): array
    {
        return $this->discountCodeData ?? [];
    }

    public function getErrors(): array
    {
        return $this->errors ?? [];
    }

    public function getNonValidReason(): string
    {
        return $this->nonValidReason ?? '';
    }

    /**
     * generate discount code data from a discount code.
     * if the code is valid, data will be generated (promoCode, endDate and discountValue).
     * If the code is not valid, then it will generate an empty array.
     */
    private function generateDiscountCodeData(string $discountCode): void
    {
        $remoteDiscountCodeData = $this->discountCodeClient->fetchDiscountData($_ENV['URL_DISCOUNT_CODE_LIST']);
        if (empty($remoteDiscountCodeData)) {
            $this->errors = $this->discountCodeClient->getErrors();

            return;
        }

        $todayDate = (new \DateTime())->format('Y-m-d');

        foreach ($remoteDiscountCodeData as $data) {
            if (true === $this->handleDiscountData($discountCode, $data, $todayDate) || null !== $this->nonValidReason) {
                return;
            }
        }

        $this->nonValidReason = DiscountCodeDictionary::NON_VALID_DISCOUNT_CODE_MISSING_REASON;
    }

    private function handleDiscountData(string $discountCode, array $data, string $todayDate): bool
    {

        if (\array_key_exists(DiscountCodeDictionary::DISCOUNT_CODE_KEY_REMOTE, $data) && $discountCode === $data[DiscountCodeDictionary::DISCOUNT_CODE_KEY_REMOTE]) {
            if ($data[DiscountCodeDictionary::END_DATE_KEY] < $todayDate) {
                $this->nonValidReason = sprintf(DiscountCodeDictionary::NON_VALID_DISCOUNT_CODE_DATE_REASON, $data[DiscountCodeDictionary::END_DATE_KEY]);

                return false;
            }

            $this->discountCodeData = [
                DiscountCodeDictionary::DISCOUNT_CODE_KEY => $discountCode,
                DiscountCodeDictionary::END_DATE_KEY => $data[DiscountCodeDictionary::END_DATE_KEY],
                DiscountCodeDictionary::DISCOUNT_VALUE_KEY => $data[DiscountCodeDictionary::DISCOUNT_VALUE_KEY],
            ];

            return true;
        }

        return false;
    }
}
