<?php

namespace App\Command\Discount;

use App\Generator\Discount\DiscountCodeOffersGenerator;
use App\Validator\Discount\DiscountCodeValidator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ValidateDiscountCodeCommand extends Command
{
    const COMMAND_DESCRIPTION = 'This command checks if a given discount code is valid. Also, gets all the attached business offers.';
    const DISCOUNT_CODE_ARGUMENT = 'discountCode';
    const ERROR_VALIDATE_GLOBAL = 'The discount code check did not went well for some reasons :';
    const ERROR_GENERATE_OFFERS_GLOBAL = 'The discount code offers generation did not went well for some reasons :';
    const SUCCESS_GLOBAL = 'The discount code checks are done now, here is what I found :';
    const SUCCESS_GENERATED_OFFERS = 'I also found some business offers for this discount code, here they are: ';

    protected static $defaultName = 'discount-code:validate';

    /** @var string */
    private $discountCode;

    /** @var DiscountCodeValidator */
    private $discountCodeValidator;

    /** @var DiscountCodeOffersGenerator */
    private $discountCodeOffersGenerator;

    /** @var SymfonyStyle */
    private $io;

    public function __construct(DiscountCodeValidator $discountCodeValidator, DiscountCodeOffersGenerator $discountCodeOffersGenerator)
    {
        $this->discountCodeValidator = $discountCodeValidator;
        $this->discountCodeOffersGenerator = $discountCodeOffersGenerator;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::COMMAND_DESCRIPTION)
            ->addArgument(
                self::DISCOUNT_CODE_ARGUMENT,
                InputArgument::REQUIRED,
                'The discount code to check'
            );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->discountCode = $input->getArgument(self::DISCOUNT_CODE_ARGUMENT);
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $isDiscountCodeValid = $this->discountCodeValidator->validate($this->discountCode);
        $errors = $this->discountCodeValidator->getErrors();

        if (!empty($errors)) {
            $this->outputErrors(self::ERROR_VALIDATE_GLOBAL, $errors);

            return Command::FAILURE;
        }


        $this->io->success(self::SUCCESS_GLOBAL);
        $this->io->text('is the code "'.$this->discountCode.'" valid ? '. ($isDiscountCodeValid ? 'YUP !' : 'NOPE !'));

        if (false === $isDiscountCodeValid) {
            $this->io->block('Reason: ' . $this->discountCodeValidator->getNonValidReason());

            return Command::SUCCESS;
        }

        $this->processDiscountCodeOffersGeneration();

        return Command::SUCCESS;
    }

    private function processDiscountCodeOffersGeneration()
    {
        $discountCodeOffers = $this->discountCodeOffersGenerator->generate($this->discountCodeValidator->getDiscountCodeData());
        $errors = $this->discountCodeValidator->getErrors();

        if (!empty($errors)) {
            $this->outputErrors(self::ERROR_GENERATE_OFFERS_GLOBAL, $errors);
        }

        if (empty($discountCodeOffers)) {
            return;
        }

        $this->io->block(self::SUCCESS_GENERATED_OFFERS);
        $this->io->writeln(json_encode($discountCodeOffers));
    }

    private function outputErrors(string $message, array $errors)
    {
        $this->io->block($message);

        foreach ($errors as $error) {
            $this->io->error($error);
        }
    }
}
