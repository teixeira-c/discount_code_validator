<?php

/**
 * @codeCoverageIgnore
 */
final class DiscountCodeDictionary
{
    const NON_VALID_DISCOUNT_CODE_MISSING_REASON = 'The discount code is missing from the list';
    const NON_VALID_DISCOUNT_CODE_DATE_REASON = 'The discount code were valid until %s';

    const DISCOUNT_CODE_KEY = 'promoCode';
    const END_DATE_KEY = 'endDate';
    const DISCOUNT_VALUE_KEY = 'discountValue';
    const DISCOUNT_CODE_KEY_REMOTE = 'code';

    const VALID_DISCOUNT_CODES_LIST_KEY_REMOTE = 'validPromoCodeList';
    const OFFER_TYPE_KEY_REMOTE = 'offerType';
    const OFFER_NAME_KEY_REMOTE = 'offerName';
    const COMPATIBLE_OFFERS_KEY = 'compatibleOfferList';
    const OFFER_TYPE_KEY = 'type';
    const OFFER_NAME_KEY = 'name';
}
